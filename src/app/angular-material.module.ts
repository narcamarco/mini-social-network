import { NgModule } from '@angular/core';

import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
    // Its okay to comment out the imports since they are the same as exports
//   imports: [
//     MatInputModule,
//     MatPaginatorModule,
//     MatCardModule,
//     MatButtonModule,
//     MatDialogModule,
//     MatToolbarModule,
//     MatExpansionModule,
//     MatProgressSpinnerModule,
//   ],
  exports: [
    MatInputModule,
    MatPaginatorModule,
    MatCardModule,
    MatButtonModule,
    MatDialogModule,
    MatToolbarModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
  ],
})
export class AngularMaterialModule {}
